import datetime
import math


def gcd_iter(u, v):
    while v:
        u, v = v, u % v
    return abs(u)


def generate_coprimes(lim=10):
    for d in range(2, lim + 1):
        for n in range(1, d):
            if gcd_iter(d, n) == 1:
                yield n, d
        d += 1


def is_square(integer):
    root = math.sqrt(integer)
    if int(root + 0.5) ** 2 == integer:
        return True
    else:
        return False


def get_minimum_dioph_bruteforce(d):
    cnt = 0
    for y, x in generate_coprimes(10 ** 8):
        cnt += 1
        if cnt % 15 ** 6 == 0:
            print(x, y)
            cnt = 0
        if x ** 2 - (d * y ** 2) == 1:
            return x, y


def compute_dioph(d):
    a = [0, 1]
    b = [1, 0]
    c = [d, 1]
    it = 1

    while True:
        q = math.floor((math.sqrt(d - c[it - 1] * c[it]) + math.sqrt(d)) / c[it])
        a.append(q * a[it] + a[it - 1])
        b.append(q * b[it] + b[it - 1])
        c.append(2 * q * math.sqrt(d - c[it - 1] * c[it]) + c[it - 1] - (q ** 2) * c[it])
        it += 1

        if a[it] ** 2 - d * b[it] ** 2 == 1:
            return a[it]


if __name__ == "__main__":
    start = datetime.datetime.now()

    maximum = 0
    D = 0

    for i in range(1, 1000):
        if is_square(i):
            continue
        x = compute_dioph(i)
        if x > maximum:
            print("New max:", x)
            maximum = x
            D = i

    print("Finally:", maximum, "d:", D)

    print("Time elapsed:", datetime.datetime.now() - start)
