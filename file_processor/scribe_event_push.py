import datetime
import gzip
import io
import json
import logging
import logging.handlers
import os
import sys
import time

from big_query_client import bqclient

import const
import scribe_event_schema
from gcsclient import GoogleCloudStorageClient

TIMESTAMP = int(time.time())


def configure_logger(logger=logging.getLogger('')):
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    console_handler = logging.StreamHandler()
    console_handler.setLevel(const.CONSOLE_LOGLEVEL)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    file_handler = logging.handlers.RotatingFileHandler(const.LOG_FILENAME, maxBytes=1024 * 1024 * 20, backupCount=5)
    file_handler.setLevel(const.FILE_LOGLEVEL)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)


def slice_and_dice(indata):
    output = {}
    # Event header
    output[u'event_id'] = indata['eventHeader'].get('eventId', None)
    # ...
    
    return output


class ScribeProcessor:
    def __init__(self, logger_name, slice_and_dice_function, load_schema, datacenter, timestamp_field='timestamp'):
        self.logger = logging.getLogger(logger_name + '.scribe')
        self.logger.info("Starting ScribeProcessor")
        self.bq_client = bqclient.BigQueryClient(const.PROJECT_ID, loggerName=const.LOGGER_NAME,
                                                 client_secrets_pwd='service_account/', service_account=True)
        self.slice_and_dice_function = slice_and_dice_function
        const.TIMESTAMP_FIELD = timestamp_field
        self.schema = load_schema
        self.datacenter = datacenter

    def filters(self, directory, processed_set):
        filter_list = []
        get_file_list_directory = self.get_file_list(directory)
        for i in get_file_list_directory:
            if const.SCRIBE_LOGS_NAME in i:
                if ".gz" in i:
                    continue
                if "current" in i:
                    continue
                if i in processed_set:
                    continue
                if const.SCRIBE_LOGS_NAME not in i:
                    continue
                filter_list.append(i)
        return sorted(filter_list)

    def process(self):
        self.logger.debug("Loading processed filelist")
        processed_set = self.load_processed_filelist()
        for directory in const.FILEDIRLIST[self.datacenter]:
            count = 0
            list_in_filter = self.filters(directory, processed_set)
            self.logger.info('Processing directory %s', directory)
            for current_file in list_in_filter:
                # Omitting last file in date
                if max(list_in_filter) in current_file:
                    continue
                self.write_file(directory, current_file)
                processed_set.add(current_file)
                self.save_filename_to_filelist(current_file)
                count += 1
            self.logger.info('Total files processed: %d', count)

        # Getting filelist to process
        filelist = self.get_file_list(const.TMPDIR)

        self.logger.debug("Found %s files to be pushed", len(filelist))

        if len(filelist) > 0:
            # Push files to GCS and remove them from temp dir
            self.push_to_gcs(const.TMPDIR, filelist)

            # Sleep to give Google time to index loaded files
            time.sleep(const.WAIT_SECS)

            # Divide files into correct tables
            filedict = {}
            for file_name in filelist:
                split = file_name.split('-')
                month = int(split[-3])
                year = int(split[-4])
                bqname = const.TABLE_NAME.format(month=month, year=year)
                if bqname not in filedict:
                    filedict[bqname] = []
                filedict[bqname].append(file_name)

            for table in filedict.keys():
                # Check if table exists. Create it if it doesn't
                tname = self.check_tables(const.DATASET_NAME, table)
                self.load_from_gcs_to_bq(filedict[tname], tname)

        self.logger.info("Cleaning Temp")
        self.clean_temp()

    def get_batch_data(self, directory, filename, batch_size=10000):
        if self.slice_and_dice_function is None:
            self.logger.critical("Slice and dice function is None!")
            sys.exit()
        self.logger.debug('Opening file %s', directory+filename)
        out = []
        savefile = None
        new_savefile = None
        try:
            with open(directory+filename) as f:
                for line in f:
                    if line == "":
                        continue
                    if len(out) >= batch_size:
                        self.logger.debug('Batch max size reached. Pushing %d rows', batch_size)
                        yield out, savefile
                        out = []
                    try:
                        data = json.loads(line)
                        processed = self.slice_and_dice_function(data)
                        if processed is None:
                            continue
                        timestamp = processed.get(const.TIMESTAMP_FIELD)
                        if timestamp is None:
                            continue
                        timestamp_date = datetime.datetime.utcfromtimestamp(timestamp)
                        month = timestamp_date.month
                        year = timestamp_date.year
                        day = timestamp_date.day
                        new_savefile = (filename[:-16]+"{year}"+"-{month:02}-"+"{day:02}").format(month=month, year=year, day=day)
                        if savefile is None:
                            savefile = new_savefile
                        if new_savefile != savefile:
                            self.logger.debug('Next month data reached. Pushing %d rows', len(out))
                            yield out, savefile
                            out = []
                            savefile = new_savefile
                        out.append(json.dumps(processed))
                    except ValueError as err:
                        self.logger.error('Error while reading file %s: %s', directory+filename, err.message)
            self.logger.debug('Pushing last batch. %d rows', len(out))
            yield out, savefile
        except IOError:
            self.logger.error('Error while gettingBatchData: %s', sys.exc_info())

    def write_file(self, directory, filename):
        self.logger.debug('Processing file: %s', filename)
        try:
            batchnum = 0
            filenum = 0
            for data, savefile in self.get_batch_data(directory, filename, const.BATCH):
                if batchnum >= const.BATCHES_PER_FILE:
                    filenum += 1
                    batchnum = 0
                if len(data) == 0:
                    continue
                tempfile = gzip.open(const.TMPDIR + savefile + '-' + str(TIMESTAMP) + str(filenum), 'ab')
                for row in data:
                    tempfile.write(row)
                    tempfile.write('\n')
                tempfile.close()
                batchnum += 1
            self.logger.debug('Finished processing file: %s', filename)
        except IOError:
            self.logger.error('Error while write_file: %s', sys.exc_info())

    def get_file_list(self, directory):
        attempts = 1
        try:
            filelist = os.listdir(directory)
            return sorted(filelist)
        except IOError:
            self.logger.error('Error while get_file_list: %s', sys.exc_info())
            if attempts < 4:
                self.logger.error('%s attempt to get list directory after one minute wait.', attempts)
                time.sleep(60)
                return self.get_file_list(directory)
            else:
                self.logger.error('Getting file list failed 3 times. Error while get_file_list: %s', sys.exc_info())

    @staticmethod
    def save_filename_to_filelist(filename):
        f = gzip.open(const.PROCESSED_FILELIST_NAME, 'ab')
        f.write(filename + '\n')
        f.close()

    @staticmethod
    def load_processed_filelist():
        out_set = set()
        f = gzip.open(const.PROCESSED_FILELIST_NAME, 'rb')
        for line in f:
            out_set.add(line[:-1])
        f.close()
        return out_set

    def check_tables(self, dataset, table_name):
        """
        Checks if table exists in dataset and creates it if not.
        """
        table_exists = False
        self.logger.debug('Getting table information')
        result = self.bq_client.list_tables(dataset)

        if result['totalItems'] != 0:
            for table in result['tables']:
                if table['tableReference']['tableId'] == table_name:
                    table_exists = True

        if not table_exists:
            self.logger.debug('Table %s does not exist in dataset %s. Creating table.', table_name, dataset)
            self.bq_client.create_table(dataset, table_name, self.schema)
        else:
            self.logger.debug('Table %s exists in dataset %s', table_name, dataset)

        return table_name

    def load_from_gcs_to_bq(self, file_list, table_name):
        load_list = []
        year = datetime.datetime.now().year
        gcspath = 'gs://dwproject-{year}/'.format(year=year)
        for logFile in file_list:
            file_path = gcspath + self.datacenter.lower() + '/' + logFile
            load_list.append(file_path)
        self.logger.info('Pushing %s files from GCS to BigQuery', len(file_list))
        response = self.bq_client.push_table_gcs(const.DATASET_NAME, table_name, self.schema, load_list)

    def push_to_gcs(self, directory, file_list):
        year = datetime.datetime.now().year
        gcs = GoogleCloudStorageClient(const.PROJECT_ID, const.LOGGER_NAME)
        for current_file in file_list:
            with open(directory + current_file, buffering=io.DEFAULT_BUFFER_SIZE) as file:
                path = self.datacenter.lower() + "/" + current_file
                self.logger.info('Pushing file %s to GCS', current_file)
                gcs.loadData('dwproject-{year}'.format(year=year), path, file)

    def clean_temp(self):
        filelist = self.get_file_list(const.TMPDIR)
        for cfile in filelist:
            try:
                self.logger.info('Removing file %s', cfile)
                os.remove(const.TMPDIR + cfile)
                self.logger.debug('Successfully removed file %s', cfile)
            except OSError as err:
                self.logger.error('Error while removing file %s: %s', cfile, err)


if __name__ == "__main__":
    start_time = datetime.datetime.now()
    logger = logging.getLogger(const.LOGGER_NAME)
    configure_logger(logger)
    logger.info('SCRIPT STARTED')

    sp = ScribeProcessor(const.LOGGER_NAME, slice_and_dice, scribe_event_schema.schema, None)
    sp.process()

    logger.info('SCRIPT FINISHED')
    logger.info('Time elapsed: %s', datetime.datetime.now() - start_time)
    logger.info('-' * 60)
