package main

import (
	"./egn_api_client"
	"bufio"
	"encoding/json"
	"log"
	"os"
	"runtime"
	"sync"
	"time"
)

const API_WORKERS = 10
const DIVIDER = 1000

const DATA_FILENAME = "data/data.json"

const FAILED_DOMAINS_LOGFILE = "log/failed_domains.log"
const GENERAL_LOGFILE = "log/data_gatherer.log"

var (
	failedDomainslogger *log.Logger
	logger              *log.Logger
)

func ApiWorker(workerId int, inChan <-chan egn_api_client.Workgroup, outChan chan<- map[string]interface{}, wait *sync.WaitGroup) {
	defer wait.Done()
	logger.Println("Starting API worker", workerId)
	for wrk := range inChan {
		api_data, err := egn_api_client.GetCustomerInfo(wrk, logger)
		if err != nil {
			logger.Println("Data dl failed for", wrk)
			failedDomainslogger.Println("API:", wrk)
		} else {
			api_data["datacenter"] = wrk.Gateway.Datacenter
			api_data["workgroup_id"] = wrk.Wgid
			outChan <- api_data
		}
	}
	logger.Println("API", workerId, "is done")
}

func SaveResultsJsonToFile(outChan <-chan map[string]interface{}, filename string, wait *sync.WaitGroup) {
	f, err := os.Create(filename)
	if err != nil {
		logger.Println("Error while creating file:", err)
		panic(err)
	}
	defer wait.Done()
	defer f.Close()

	writer := bufio.NewWriter(f)

	for res := range outChan {
		jsonned, err := json.Marshal(res)
		if err != nil {
			logger.Println("Error while creating json:", err)
		}
		_, err = writer.Write(jsonned)
		if err != nil {
			logger.Println("Error while writing data:", err)
		}
		_, err = writer.WriteString("\n")
		if err != nil {
			logger.Println("Error while writing data:", err)
		}
	}
	writer.Flush()
	logger.Println("Finished writing to", filename+". Exiting.")
}

func ProvideApiQueue() chan egn_api_client.Workgroup {
	wgidChan := make(chan egn_api_client.Workgroup)
	go func() {
		defer close(wgidChan)
		cnt := 0
		wgidList := egn_api_client.GetAllWorkgroups(logger)
		for _, wg := range wgidList {
			wgidChan <- wg
			cnt++
			if cnt%DIVIDER == 0 {
				logger.Println("API:", cnt, "/", len(wgidList))
			}
		}
	}()

	return wgidChan
}

func GatherData(wait *sync.WaitGroup) {
	defer wait.Done()
	logger.Println("Starting data gathering")
	var wg sync.WaitGroup
	var writewait sync.WaitGroup

	wgidsApiChan := ProvideApiQueue()
	outApiChan := make(chan map[string]interface{})

	logger.Println("Starting", API_WORKERS, "workers")
	for i := 0; i < API_WORKERS; i++ {
		wg.Add(1)
		go ApiWorker(i, wgidsApiChan, outApiChan, &wg)
	}
	go SaveResultsJsonToFile(outApiChan, DATA_FILENAME, &writewait)
	writewait.Add(1)

	wg.Wait()
	close(outApiChan)

	writewait.Wait()

	logger.Println("Finished data gathering")
}

func configureloggers() {
	failed_file, err := os.OpenFile(FAILED_DOMAINS_LOGFILE, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		logger.Fatalln("Failed to open failed_domains log file:", err)
	}

	loggerger_file, err := os.OpenFile(GENERAL_LOGFILE, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		logger.Fatalln("Failed to open data_gatherer log file:", err)
	}

	failedDomainslogger = log.New(failed_file, "Faillogger:", log.Lshortfile|log.LstdFlags)
	logger = log.New(loggerger_file, "Log:", log.Lshortfile|log.LstdFlags)
}

func main() {
	configureloggers()
	logger.Println("----------------------------------")
	logger.Println("Starting Data Gatherer")
	startTime := time.Now()
	var wg sync.WaitGroup

	configureloggers()

	go GatherData(&wg)
	wg.Add(1)

	wg.Wait()

	logger.Println("Script finished. Time elapsed:", time.Since(startTime))
	logger.Println("----------------------------------")
}
