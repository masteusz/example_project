package egn_api_client

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
)

const DOMAINS_ENDPOINT = ""
const CUSTOMER_INFO_ENDPOINT = ""
const SEC_KEY = ""

type Gateway struct {
	Url        string
	Datacenter string
}

type Workgroup struct {
	Wgid    string
	Gateway Gateway
}

var GATEWAY_LIST = [...]Gateway{
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
	Gateway{Url: "http://", Datacenter: ""},
}

func GetWorkgroupsFromGateway(gtw Gateway, outChan chan Workgroup, wait *sync.WaitGroup, logger *log.Logger) error {
	defer wait.Done()
	logger.Println("Getting workgroups from", gtw.Url)
	client := http.DefaultClient

	reqUrl := gtw.Url + DOMAINS_ENDPOINT + SEC_KEY

	request, err := http.NewRequest("GET", reqUrl, nil)
	if err != nil {
		logger.Println("Error:", err)
		return err
	}

	request.Header.Add("Accept", "application/json")

	res, err := client.Do(request)
	if err != nil {
		logger.Println("Error:", err)
		return err
	}

	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		logger.Println("Error:", err)
		return err
	}
	var workgroupMap map[string]interface{}
	err = json.Unmarshal([]byte(body), &workgroupMap)
	if err != nil {
		logger.Println("Error:", err)
		return err
	}
	rawWrkList := workgroupMap["workgroups"].([]interface{})
	count := 0
	for _, wrkgrp := range rawWrkList {
		werk := wrkgrp.(map[string]interface{})
		id := werk["workgroup_id"].(string)
		entry := Workgroup{Wgid: id, Gateway: gtw}
		outChan <- entry
		count++
	}
	logger.Println("Received", count, "workgroups from", gtw.Url)
	return nil
}

func GetAllWorkgroups(logger *log.Logger) []Workgroup {
	var wg sync.WaitGroup
	wgList := make([]Workgroup, 0)

	recvChannel := make(chan Workgroup)
	for _, gateElem := range GATEWAY_LIST {
		wg.Add(1)
		go GetWorkgroupsFromGateway(gateElem, recvChannel, &wg, logger)
	}
	go func() {
		wg.Wait()
		close(recvChannel)
	}()

	for received := range recvChannel {
		wgList = append(wgList, received)
	}

	return wgList
}

func GetCustomerInfo(wg Workgroup, logger *log.Logger) (map[string]interface{}, error) {
	client := http.DefaultClient

	req := wg.Gateway.Url + CUSTOMER_INFO_ENDPOINT + wg.Wgid + SEC_KEY
	request, err := http.NewRequest("GET", req, nil)
	if err != nil {
		logger.Println("Error:", err)
		return nil, err
	}

	request.Header.Add("Accept", "application/json")

	res, err := client.Do(request)
	if err != nil {
		logger.Println("Error:", err)
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()

	if err != nil {
		logger.Println("Error:", err)
		return nil, err
	}
	var customerMap map[string]interface{}
	err = json.Unmarshal([]byte(body), &customerMap)
	if err != nil {
		logger.Println("Error:", err)
		return nil, err
	}
	return customerMap, nil
}
